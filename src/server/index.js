const csv = require('csv-parser');
const fs = require('fs');
const matchesPerYear = require('./1-matches-per-year');
const matchesWonPerTeamPerYear = require('./2-matches-won-per-team-per-year');
const extraRunsPerTeam = require('./3-extra-run-per-team-year-2016'); 
const topTenEconomicalBowler = require('./4-economical-bowlers-in-2015');
const totaltimeseachteamwontossandmatch= require('./5-total-times-each-team-won-toss-and-match')
const highestPomPerSeason = require('./6-year-wise-playerOfMatch-each-season');
const batsmanStrikeRateSeason = require('./7-strike-rate-of-batsMan-each-session');
const playerDismissedAnotherPlayer = require('./8-highest-num-of-one-player-dismiss-by-another-player');
const mostEconomicBowler = require('./9-bowler-best-economy-in-super-over');

csvReadJsonWrite(matchesPerYear, 'matchesPerYear');
csvReadJsonWrite(matchesWonPerTeamPerYear, 'matches-won-per-team-per-year');
csvReadJsonWrite(extraRunsPerTeam, 'extra-run-per-team-year-2016');
csvReadJsonWrite(topTenEconomicalBowler, 'economical-bowlers-in-2015');
csvReadJsonWrite(totaltimeseachteamwontossandmatch, 'total-times-each-team-won-toss-and-match');
csvReadJsonWrite(highestPomPerSeason, 'year-wise-playerOfMatch-each-season');
csvReadJsonWrite(batsmanStrikeRateSeason, 'strike-rate-of-batsMan-each-session');
csvReadJsonWrite(playerDismissedAnotherPlayer, 'highest-num-of-one-player-dismiss-by-another-player');
csvReadJsonWrite(mostEconomicBowler, 'bowler-best-economy-in-super-over');

function csvReadJsonWrite(cb, path) {
  const matches = [];
  let deliveries = [];

  fs.createReadStream('../data/matches.csv')
    .pipe(csv({}))
    .on('data', (data) => matches.push(data))
    .on('end', () => {
      fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {
          fs.writeFile(
            `../public/output/${path}.json`,
            JSON.stringify(cb(matches, deliveries)),
            (err) => {
              if (err) throw err;
              console.log('Done Saving :)');
            },
          );
        });
    });
}