function totaltimeeachteamwontossandmatch(matches) {
  const wonTossWonMatchResult = matches.reduce(function countTossWinsMatchWins(
    tossWinsMatchWinsObj,
    match
  ) {
    if (match.toss_winner === match.winner) {
      if (match.winner in tossWinsMatchWinsObj) {
        tossWinsMatchWinsObj[match.winner]++;
      } else {
        tossWinsMatchWinsObj[match.winner] = 1;
      }
    }

    return tossWinsMatchWinsObj;
  },
  {});

  //Object containing number of times team won the toss and won the match.
  return wonTossWonMatchResult;
}
module.exports = totaltimeeachteamwontossandmatch;
