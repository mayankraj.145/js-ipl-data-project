function findMaxDismissals(_, deliveriesData) {
  const dismissalPairs = {};

  for (const delivery of deliveriesData) {
    const dismissed = delivery["player_dismissed"];
    const bowler = delivery["bowler"];

    if (dismissed && bowler) {
      const pairKey = `${dismissed}-${bowler}`;

      if (!dismissalPairs[pairKey]) {
        dismissalPairs[pairKey] = 1;
      } else {
        dismissalPairs[pairKey]++;
      }
    }
  }

  let maxDismissals = 0;
  let mostDismissedPlayer = null;
  let mostFrequentDismissingBowler = null;

  for (const pairKey in dismissalPairs) {
    if (dismissalPairs[pairKey] > maxDismissals) {
      maxDismissals = dismissalPairs[pairKey];
      const [dismissedPlayer, dismissingBowler] = pairKey.split("-");
      mostDismissedPlayer = dismissedPlayer;
      mostFrequentDismissingBowler = dismissingBowler;
    } else if (dismissalPairs[pairKey] === maxDismissals) {
      // If there is a tie for max dismissals, add both players
      const [dismissedPlayer, dismissingBowler] = pairKey.split("-");
      if (mostDismissedPlayer !== dismissedPlayer) {
        mostDismissedPlayer += `, ${dismissedPlayer}`;
      }
      if (mostFrequentDismissingBowler !== dismissingBowler) {
        mostFrequentDismissingBowler += `, ${dismissingBowler}`;
      }
    }
  }

  if (mostDismissedPlayer) {
    return {
      mostDismissedPlayer,
      mostFrequentDismissingBowler,
      dismissalCount: maxDismissals,
    };
  }

  return null;
}

module.exports = findMaxDismissals;
