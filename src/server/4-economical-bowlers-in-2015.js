function topTenEconomicalBowlers(matches, deliveries, year = 2015) {
  const yearMatches = matches.filter((match) => match.season === String(year));

  const bowlerStats = {};

  deliveries
    .filter((delivery) =>
      yearMatches.some((match) => match.id === delivery.match_id)
    )
    .forEach((delivery) => {
      const bowler = delivery.bowler;
      const runs = Number(delivery.total_runs);
      const isWideOrNoBall =
        Number(delivery.wide_runs) > 0 || Number(delivery.noball_runs) > 0;

      if (!bowlerStats[bowler]) {
        bowlerStats[bowler] = { runs: 0, balls: 0 };
      }

      if (!isWideOrNoBall) {
        bowlerStats[bowler].balls++;
        bowlerStats[bowler].runs += runs;
      }
    });

  const topTenBowlers = Object.entries(bowlerStats)
    .map(([bowler, stats]) => ({
      name: bowler,
      economy:
        stats.balls > 0
          ? Number((stats.runs / (stats.balls / 6)).toFixed(2))
          : 0,
    }))
    .filter((bowler) => !isNaN(bowler.economy)) // Filter out bowlers with NaN economy
    .sort((bowler1, bowler2) => bowler1.economy - bowler2.economy)
    .slice(0, 10);

  return topTenBowlers;
}

module.exports = topTenEconomicalBowlers;
