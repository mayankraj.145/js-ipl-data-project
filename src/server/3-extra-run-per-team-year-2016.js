function extraRunsIn2016(matchesData, deliveriesData) {
  const extraRunsConceded = {};

  const matchesIn2016 = matchesData.filter(
    (match) => match["season"] === "2016"
  );

  for (const match of matchesIn2016) {
    const matchId = match["id"];

    const deliveriesForMatch = deliveriesData.filter(
      (delivery) => delivery["match_id"] === matchId
    );

    for (const delivery of deliveriesForMatch) {
      const bowlingTeam = delivery["bowling_team"];
      const extras = parseInt(delivery["extra_runs"], 10);

      if (!extraRunsConceded[bowlingTeam]) {
        extraRunsConceded[bowlingTeam] = extras;
      } else {
        extraRunsConceded[bowlingTeam] += extras;
      }
    }
  }

  return extraRunsConceded;
}

module.exports = extraRunsIn2016;
