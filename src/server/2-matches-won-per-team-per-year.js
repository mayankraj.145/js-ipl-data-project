function matchesWonPerTeamPerYear(matches) {
  const totalMatchesWonPerTeamPerYear = matches.reduce(
    function countMatchWonPerTeamPerYear(matchesWonPerTeamPerYearObj, match) {
      if (match.winner !== "") {
        if (!(match.winner in matchesWonPerTeamPerYearObj)) {
          matchesWonPerTeamPerYearObj[match.winner] = {};
        }
        if (!(match.season in matchesWonPerTeamPerYearObj[match.winner])) {
          matchesWonPerTeamPerYearObj[match.winner][match.season] = 0;
        }

        matchesWonPerTeamPerYearObj[match.winner][match.season]++;
      }

      return matchesWonPerTeamPerYearObj;
    },
    {}
  );

  //object containing total matches won by each team per year
  return totalMatchesWonPerTeamPerYear;
}

module.exports = matchesWonPerTeamPerYear;
