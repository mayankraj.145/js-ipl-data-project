const highestPomPerSeason = require("../src/server/6-year-wise-playerOfMatch-each-season");

const sampleMatches = [
  { season: "2010", player_of_match: "John" },
  { season: "2010", player_of_match: "Mike" },
  { season: "2010", player_of_match: "David" },
  { season: "2010", player_of_match: "David" },
  { season: "2011", player_of_match: "Mike" },
  { season: "2011", player_of_match: "Steve" },
  { season: "2011", player_of_match: "Steve" },
  { season: "2011", player_of_match: "Steve" },
  { season: "2011", player_of_match: "David" },
  { season: "2012", player_of_match: "David" },
  { season: "2012", player_of_match: "John" },
  { season: "2012", player_of_match: "Steve" },
  { season: "2012", player_of_match: "John" },
  { season: "2012", player_of_match: "Steve" },
  { season: "2013", player_of_match: "David" },
  { season: "2013", player_of_match: "Mike" },
  { season: "2013", player_of_match: "David" },
  { season: "2013", player_of_match: "Steve" },
  { season: "2013", player_of_match: "Mike" },
];

test("highest player of the match for each season", () => {
  expect(highestPomPerSeason(sampleMatches)).toMatchObject({
    2010: { David: 2 },
    2011: { Steve: 3 },
    2012: { John: 2, Steve: 2 },
    2013: { David: 2, Mike: 2 },
  });
});
