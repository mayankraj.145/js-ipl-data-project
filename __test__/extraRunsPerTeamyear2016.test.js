const extraRunsPerTeamyear2016 = require("../src/server/3-extra-run-per-team-year-2016");
const sampleMatches = [
  { id: 2, season: "2016" },
  { id: 3, season: "2018" },
  { id: 4, season: "2016" },
  { id: 1, season: "2017" },
  { id: 5, season: "2016" },
  { id: 6, season: "2018" },
  { id: 7, season: "2016" },
  { id: 8, season: "2018" },
];

const sampleDeliveries = [
  { match_id: 1, extra_runs: 2, bowling_team: "TeamA" },
  { match_id: 1, extra_runs: 2, bowling_team: "TeamB" },
  { match_id: 2, extra_runs: 0, bowling_team: "TeamC" },
  { match_id: 2, extra_runs: 4, bowling_team: "TeamA" },
  { match_id: 3, extra_runs: 3, bowling_team: "TeamC" },
  { match_id: 3, extra_runs: 2, bowling_team: "TeamB" },
  { match_id: 4, extra_runs: 0, bowling_team: "TeamC" },
  { match_id: 4, extra_runs: 2, bowling_team: "TeamB" },
  { match_id: 5, extra_runs: 6, bowling_team: "TeamC" },
  { match_id: 5, extra_runs: 2, bowling_team: "TeamA" },
  { match_id: 6, extra_runs: 10, bowling_team: "TeamB" },
  { match_id: 6, extra_runs: 2, bowling_team: "TeamC" },
  { match_id: 7, extra_runs: 6, bowling_team: "TeamB" },
  { match_id: 7, extra_runs: 2, bowling_team: "TeamC" },
  { match_id: 7, extra_runs: 1, bowling_team: "TeamB" },
  { match_id: 8, extra_runs: 2, bowling_team: "TeamC" },
  { match_id: 8, extra_runs: 7, bowling_team: "TeamA" },
  { match_id: 8, extra_runs: 13, bowling_team: "TeamC" },
  { match_id: 8, extra_runs: 2, bowling_team: "TeamB" },
];

test("calculate extra runs per team in different seasons", () => {
  expect(
    extraRunsPerTeamyear2016(sampleMatches, sampleDeliveries)
  ).toMatchObject({
    TeamA: 6,
    TeamB: 9,
    TeamC: 8,
  });
});
