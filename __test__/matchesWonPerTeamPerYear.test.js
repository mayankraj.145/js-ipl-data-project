const matchesWonPerTeamPerYear = require("../src/server/2-matches-won-per-team-per-year");
const sample = [
  {
    season: "2001",
    winner: "bombay",
  },
  {
    season: "2001",
    winner: "bombay",
  },
  {
    season: "2002",
    winner: "pune",
  },
  {
    season: "2002",
    winner: "bombay",
  },
  {
    season: "2002",
    winner: "patna",
  },
  {
    season: "2003",
    winner: "patna",
  },
  {
    season: "2003",
    winner: "bombay",
  },
  {
    season: "2004",
    winner: "patna",
  },
  {
    season: "2004",
    winner: "bombay",
  },
  {
    season: "2004",
    winner: "pune",
  },
];

test("calculate matches per year", () => {
  expect(matchesWonPerTeamPerYear(sample)).toMatchObject({
    bombay: {
      2001: 2,
      2002: 1,
      2003: 1,
      2004: 1,
    },
    patna: {
      2002: 1,
      2003: 1,
      2004: 1,
    },
    pune: {
      2002: 1,
      2004: 1,
    },
  });
});
