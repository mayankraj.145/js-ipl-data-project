const findMaxDismissals = require("../src/server/8-highest-num-of-one-player-dismiss-by-another-player");

const sampleDeliveries = [
  { player_dismissed: "PlayerA", bowler: "Bowler1" },
  { player_dismissed: "PlayerA", bowler: "Bowler2" },
  { player_dismissed: "PlayerB", bowler: "Bowler1" },
  { player_dismissed: "PlayerB", bowler: "Bowler2" },
  { player_dismissed: "PlayerB", bowler: "Bowler1" },
  { player_dismissed: "PlayerC", bowler: "Bowler2" },
];

test("find player with the most dismissals", () => {
  expect(findMaxDismissals(null, sampleDeliveries)).toMatchObject({
    mostDismissedPlayer: "PlayerB",
    mostFrequentDismissingBowler: "Bowler1",
    dismissalCount: 2,
  });
});
