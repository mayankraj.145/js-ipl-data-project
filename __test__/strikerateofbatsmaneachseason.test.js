const batsmanStrikeRateSeason = require("../src/server/7-strike-rate-of-batsMan-each-session");

const sampleMatches = [
  { id: 1, season: "2015" },
  { id: 2, season: "2015" },
  { id: 3, season: "2015" },
  { id: 4, season: "2016" },
  { id: 5, season: "2016" },
  { id: 6, season: "2016" },
  { id: 7, season: "2016" },
  { id: 8, season: "2016" },
];

const sampleDeliveries = [
  { match_id: 1, batsman: "Dhoni", batsman_runs: "1", wide_runs: "0" },
  { match_id: 1, batsman: "Raina", batsman_runs: "2", wide_runs: "0" },
  { match_id: 2, batsman: "Kohli", batsman_runs: "3", wide_runs: "0" },
  { match_id: 2, batsman: "Rohit", batsman_runs: "4", wide_runs: "0" },
  { match_id: 3, batsman: "Smith", batsman_runs: "4", wide_runs: "0" },
  { match_id: 3, batsman: "Warner", batsman_runs: "3", wide_runs: "0" },
  { match_id: 4, batsman: "AB de Villiers", batsman_runs: "4", wide_runs: "0" },
  { match_id: 4, batsman: "Jadeja", batsman_runs: "4", wide_runs: "0" },
  { match_id: 5, batsman: "Sharma", batsman_runs: "1", wide_runs: "0" },
  { match_id: 5, batsman: "Pandya", batsman_runs: "2", wide_runs: "0" },
  { match_id: 6, batsman: "Dhoni", batsman_runs: "4", wide_runs: "0" },
  { match_id: 6, batsman: "Shami", batsman_runs: "4", wide_runs: "0" },
  { match_id: 7, batsman: "Raina", batsman_runs: "3", wide_runs: "0" },
  { match_id: 7, batsman: "Kohli", batsman_runs: "1", wide_runs: "0" },
  { match_id: 7, batsman: "Raina", batsman_runs: "2", wide_runs: "0" },
  { match_id: 8, batsman: "Dhoni", batsman_runs: "3", wide_runs: "0" },
  { match_id: 8, batsman: "Shami", batsman_runs: "2", wide_runs: "0" },
  { match_id: 8, batsman: "Raina", batsman_runs: "1", wide_runs: "0" },
  { match_id: 8, batsman: "Kohli", batsman_runs: "1", wide_runs: "0" },
];

test("batsman strike rate per season", () => {
  expect(
    batsmanStrikeRateSeason(sampleMatches, sampleDeliveries)
  ).toMatchObject({
    Dhoni: {
      2015: 100.0,
      2016: 350.0,
    },
    Raina: {
      2015: 200.0,
      2016: 200.0,
    },
    Kohli: {
      2015: 300.0,
      2016: 100.0,
    },
    Rohit: {
      2015: 400.0,
    },
    Smith: {
      2015: 400.0,
    },
    Warner: {
      2015: 300.0,
    },
    "AB de Villiers": {
      2016: 400.0,
    },
    Jadeja: {
      2016: 400.0,
    },
    Sharma: {
      2016: 100.0,
    },
    Pandya: {
      2016: 200.0,
    },
    Shami: {
      2016: 300.0,
    },
  });
});
