const wonTossWonMatch = require("../src/server/5-total-times-each-team-won-toss-and-match");
const sample = [
  { winner: "teamA", toss_winner: "teamB" },
  { winner: "teamC", toss_winner: "teamC" },
  { winner: "teamA", toss_winner: "teamA" },
  { winner: "teamB", toss_winner: "teamB" },
  { winner: "teamC", toss_winner: "teamC" },
  { winner: "teamA", toss_winner: "teamB" },
  { winner: "teamA", toss_winner: "teamC" },
  { winner: "teamA", toss_winner: "teamB" },
  { winner: "teamC", toss_winner: "teamB" },
];

test("number of times each team won the toss and won the match", () => {
  expect(wonTossWonMatch(sample)).toMatchObject({
    teamA: 1,
    teamB: 1,
    teamC: 2,
  });
});
