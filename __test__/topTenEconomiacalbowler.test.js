const topTenEconomicalBowler = require("../src/server/4-economical-bowlers-in-2015");

const sampleMatches = [
  { id: 1, season: "2015" },
  { id: 2, season: "2015" },
  { id: 3, season: "2015" },
  { id: 4, season: "2015" },
  { id: 5, season: "2015" },
  { id: 6, season: "2015" },
  { id: 7, season: "2015" },
  { id: 8, season: "2015" },
];

const sampleDeliveries = [
  { match_id: 1, bowler: "Dhoni", total_runs: "1", wide_runs: "0" },
  { match_id: 1, bowler: "Raina", total_runs: "2", wide_runs: "0" },
  { match_id: 2, bowler: "Kohli", total_runs: "3", wide_runs: "0" },
  { match_id: 2, bowler: "Rohit", total_runs: "4", wide_runs: "0" },
  { match_id: 3, bowler: "Smith", total_runs: "4", wide_runs: "0" },
  { match_id: 3, bowler: "Warner", total_runs: "3", wide_runs: "0" },
  { match_id: 4, bowler: "AB", total_runs: "4", wide_runs: "0" },
  { match_id: 4, bowler: "Jadeja", total_runs: "4", wide_runs: "0" },
  { match_id: 5, bowler: "Sharma", total_runs: "1", wide_runs: "0" },
  { match_id: 5, bowler: "Pandya", total_runs: "2", wide_runs: "0" },
  { match_id: 6, bowler: "Dhoni", total_runs: "4", wide_runs: "0" },
  { match_id: 6, bowler: "Shami", total_runs: "4", wide_runs: "0" },
  { match_id: 7, bowler: "Raina", total_runs: "3", wide_runs: "0" },
  { match_id: 7, bowler: "Kohli", total_runs: "1", wide_runs: "0" },
  { match_id: 7, bowler: "Raina", total_runs: "2", wide_runs: "0" },
  { match_id: 8, bowler: "Dhoni", total_runs: "3", wide_runs: "0" },
  { match_id: 8, bowler: "Shami", total_runs: "2", wide_runs: "0" },
  { match_id: 8, bowler: "Raina", total_runs: "1", wide_runs: "0" },
  { match_id: 8, bowler: "Kohli", total_runs: "1", wide_runs: "0" },
];

// console.log(topTenEconomicalBowler(sampleMatches, sampleDeliveries));

test("top ten economical bowler in 2015", () => {
  expect(topTenEconomicalBowler(sampleMatches, sampleDeliveries)).toMatchObject(
    [
      { name: "Sharma", economy: 6 },
      { name: "Kohli", economy: 10 },
      { name: "Raina", economy: 12 },
      { name: "Pandya", economy: 12 },
      { name: "Dhoni", economy: 16 },
      { name: "Warner", economy: 18 },
      { name: "Shami", economy: 18 },
      { name: "Rohit", economy: 24 },
      { name: "Smith", economy: 24 },
      { name: "AB", economy: 24 },
    ]
  );
});
