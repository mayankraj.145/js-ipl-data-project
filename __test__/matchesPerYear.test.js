const matchesPerYear = require("../src/server/1-matches-per-year");

const sample = [
  {
    season: "2001",
  },
  {
    season: "2001",
  },
  {
    season: "2004",
  },
  {
    season: "2004",
  },
  {
    season: "2004",
  },
  {
    season: "1990",
  },
  {
    season: "1990",
  },
  {
    season: "2000",
  },
  {
    season: "2000",
  },
  {
    season: "2000",
  },
];
test("calculate matches per year", () => {
  expect(matchesPerYear(sample)).toMatchObject({
    2001: 2,
    2004: 3,
    1990: 2,
    2000: 3,
  });
});
